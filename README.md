# MundoProyectoLaravel

El presente proyecto trabaja junto a  <b> ProyectoMundoReact(https://gitlab.com/dvelasco183/proyectomundoreact) </b> 


<h3>Sistemas Necesarios</h3>

-Composer versión 2.0 <br>
-Xampp versión 8.0 <br>
-MySQL Workbench <br>


<h3>Implementacion</h3>

1) Iniciar Xampp junto con el modulo MySQL
2) Crear un esquema en Workbench con el nombre <b> mundodatodb </b>
3) Cambiar el nombre del archivo env.example a .env
4) Editar este archivo cambiando el valor de la variable DB_DATABASE a <b> mundodatodb </b>
5) Ejecutar los siguientes comando en el proyecto:

<code>
composer install  <br>
php artisan migrate <br>
php artisan db:seed <br>
</code>


6) Ejecutar el proyecto con el comando <code>php artisan serve </code>