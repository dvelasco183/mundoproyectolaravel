<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class CiudadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("ciudades")->Insert([

            [
                "nombre" => "Ciudad 1",
                "provincia_id" => "1"
            ],
            [
                "nombre" => "Ciudad 2",
                "provincia_id" => "1"
            ],

            [
                "nombre" => "Ciudad 3",
                "provincia_id" => "2"
            ],
            [
                "nombre" => "Ciudad 4",
                "provincia_id" => "2"
            ],

            [
                "nombre" => "Ciudad 5",
                "provincia_id" => "3"
            ],
            [
                "nombre" => "Ciudad 6",
                "provincia_id" => "3"
            ],

            [
                "nombre" => "Ciudad 7",
                "provincia_id" => "4"
            ],
            [
                "nombre" => "Ciudad 8",
                "provincia_id" => "4"
            ],

            [
                "nombre" => "Ciudad 9",
                "provincia_id" => "5"
            ],
            [
                "nombre" => "Ciudad 10",
                "provincia_id" => "5"
            ],

            [
                "nombre" => "Ciudad 11",
                "provincia_id" => "6"
            ],
            [
                "nombre" => "Ciudad 12",
                "provincia_id" => "6"
            ],

        ]);
    }
}
