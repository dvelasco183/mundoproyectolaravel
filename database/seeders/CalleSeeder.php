<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class CalleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("calles")->Insert([
            [
                "nombre" => "Calle 1",
                "ciudad_id" => "1"
            ],
            [
                "nombre" => "Calle 2",
                "ciudad_id" => "1"
            ],

            [
                "nombre" => "Calle 3",
                "ciudad_id" => "2"
            ],
            [
                "nombre" => "Calle 4",
                "ciudad_id" => "2"
            ],

            [
                "nombre" => "Calle 5",
                "ciudad_id" => "3"
            ],
            [
                "nombre" => "Calle 6",
                "ciudad_id" => "3"
            ],

            [
                "nombre" => "Calle 7",
                "ciudad_id" => "4"
            ],
            [
                "nombre" => "Calle 8",
                "ciudad_id" => "4"
            ],

            [
                "nombre" => "Calle 9",
                "ciudad_id" => "5"
            ],
            [
                "nombre" => "Calle 10",
                "ciudad_id" => "5"
            ],

            [
                "nombre" => "Calle 11",
                "ciudad_id" => "6"
            ],
            [
                "nombre" => "Calle 12",
                "ciudad_id" => "6"
            ],

            [
                "nombre" => "Calle 13",
                "ciudad_id" => "7"
            ],
            [
                "nombre" => "Calle 14",
                "ciudad_id" => "7"
            ],

            [
                "nombre" => "Calle 15",
                "ciudad_id" => "8"
            ],
            [
                "nombre" => "Calle 16",
                "ciudad_id" => "8"
            ],

            [
                "nombre" => "Calle 17",
                "ciudad_id" => "9"
            ],
            [
                "nombre" => "Calle 18",
                "ciudad_id" => "9"
            ],

            [
                "nombre" => "Calle 19",
                "ciudad_id" => "10"
            ],
            [
                "nombre" => "Calle 20",
                "ciudad_id" => "10"
            ],

            [
                "nombre" => "Calle 21",
                "ciudad_id" => "11"
            ],
            [
                "nombre" => "Calle 22",
                "ciudad_id" => "11"
            ],

            [
                "nombre" => "Calle 23",
                "ciudad_id" => "12"
            ],
            [
                "nombre" => "Calle 24",
                "ciudad_id" => "12"
            ],
        ]);
    }
}
