<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class ProvinciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("provincias")->Insert([

            [
                "nombre" => "Provincia 1",
                "region_id" => "1"
            ],
            [
                "nombre" => "Provincia 2",
                "region_id" => "1"
            ],

            [
                "nombre" => "Provincia 3",
                "region_id" => "2"
            ],
            [
                "nombre" => "Provincia 4",
                "region_id" => "2"
            ],

            [
                "nombre" => "Provincia 5",
                "region_id" => "3"
            ],
            [
                "nombre" => "Provincia 6",
                "region_id" => "3"
            ],
          
        ]);
    }
}
