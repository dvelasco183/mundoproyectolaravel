<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("regiones")->Insert([

            [
                "nombre" => "Región 1"
            ],
            [
                "nombre" => "Región 2"
            ],
            [
                "nombre" => "Región 3"
            ]
        ]);
    }
}
