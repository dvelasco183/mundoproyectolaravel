<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DatosApiController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/InsertCalles', [DatosApiController::class, 'InsertCalles']);
Route::put('/ModificarCalles', [DatosApiController::class, 'ModificarCalles']);


Route::get('/DatosRegion', [DatosApiController::class, 'DatosRegion']);
Route::post('/DatosProvincia', [DatosApiController::class, 'DatosProvincia']);
Route::post('/DatosCiudad', [DatosApiController::class, 'DatosCiudad']);
Route::post('/DatosCalle', [DatosApiController::class, 'DatosCalle']);