<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Calles;
use App\Models\Ciudades;
use App\Models\Regiones;
use App\Models\Provincias;

class DatosApiController extends Controller
{
    public function InsertCalles(Request $request)
    {
        $DatosCalle =  json_decode($request->getContent(), true);
        $calle = new Calles();
        $calle->ciudad_id = intval($DatosCalle["idCiudad"]);
        $calle->nombre = $DatosCalle["nombreCalle"];
        $calle->save();
        return response()->json([
            "action" => "inserted",
            "tid" => $calle->id
        ]);
    }

    public function ModificarCalles(Request $request)
    {
        $DatosCalle =  json_decode($request->getContent(), true);
        $calle = Calles::find(intval($DatosCalle["idCalle"]));
        $calle->nombre = $DatosCalle["nombreCalle"];
        $calle->save();
        return response()->json([
            "action" => "updated",
            "tid" => $calle->id
        ]);
    }

    public function DatosRegion()
    {
        $response = Regiones::all();
        return (json_encode($response));
    }

    public function DatosProvincia(Request $request)
    {

        $idRegion =  json_decode($request->getContent(), true);
        $response = Provincias::where('region_id', $idRegion)->get();
        return (json_encode($response));
    }

    public function DatosCiudad(Request $request)
    {
        $idProvincia =  json_decode($request->getContent(), true);
        $response = Ciudades::where('provincia_id', $idProvincia)->get();
        return (json_encode($response));
    }

    public function DatosCalle(Request $request)
    {
        $ciudad_id =  json_decode($request->getContent(), true);
        $response = Calles::where('ciudad_id', $ciudad_id)->get();
        return (json_encode($response));
    }
}
